import tensorflow as tf
import tf2onnx
import onnx

# Define the custom Swish activation function
@tf.function
def custom_swish(x):
        return x / (1 + tf.exp(-x))
       

# Register the custom Swish activation function
tf.keras.utils.get_custom_objects().update({'swish': custom_swish})

# Load the model from an H5 file
model = tf.keras.models.load_model("model.h5", compile=False)

# Convert the model to an ONNX model with custom Swish activation
onnx_model, _ = tf2onnx.convert.from_keras(
            model,
            opset=13,
            custom_ops={"swish": custom_swish},
            )

# Save the ONNX model to a file
onnx.save(onnx_model, "generator_operators.onnx")
