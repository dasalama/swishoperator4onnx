import tensorflow as tf
import tf2onnx
import onnx

# Define the custom Swish activation function
@tf.function
def custom_swish(x):
        return x * tf.sigmoid(x)
       

# Register the custom Swish activation function
tf.keras.utils.get_custom_objects().update({'swish': custom_swish})

# Load the model from an H5 file
model = tf.keras.models.load_model("model.h5", compile=False)

# Convert the model to an ONNX model with custom Swish activation
onnx_model, _ = tf2onnx.convert.from_keras(
            model,
            opset=13,
            custom_ops={"swish": custom_swish},
            )

# Loop through the nodes in the ONNX model and replace any Sigmoid operators with Swish operators
for node in onnx_model.graph.node:
    if node.op_type == "Sigmoid":
        print (node.op_type)
        swish_node = onnx.helper.make_node(
            "Swish",
            inputs=node.input,
            outputs=node.output,
            name=node.name,
            domain="com.example",
        )
        onnx_model.graph.node.remove(node)
        onnx_model.graph.node.extend([swish_node])


# Print the list of node names
for node in onnx_model.graph.node:
        activation_function_name = node.op_type
        print(f"{activation_function_name}.")


# Save the ONNX model to a file
onnx.save(onnx_model, "generator_swish.onnx")
